#include "SnakeClass.h"

int main()
{
	Snake s;

	while (!s.getGameOver()) {
		s.draw();
		s.input();
		s.logic();
		Sleep(10);
	}
	
	system("pause");
	return 0;
}