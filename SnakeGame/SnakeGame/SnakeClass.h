#pragma once
#include<iostream>
#include<conio.h>
#include<vector>
#include<Windows.h>

enum Directions {
	STOP,
	LEFT,
	RIGHT,
	UP,
	DOWN
};

class Snake {
	Directions dir;
	bool gameOver;
	int width, height;
	int snakeX, snakeY, fruitX, fruitY, score;
	int tailX[100], tailY[100], nTail;
public:
	Snake();
	void draw();
	void input();
	void logic();
	bool getGameOver();
};
