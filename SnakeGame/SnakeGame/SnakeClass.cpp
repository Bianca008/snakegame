#include "SnakeClass.h"

Snake::Snake() {
	dir = STOP;
	gameOver = false;
	width = 20;
	height = 20;
	score = 0;
	nTail = 0;
	snakeX = width / 2;
	snakeY = height / 2;
	fruitX = rand() % width;
	fruitY = rand() % height;
}

void Snake::draw() {
	system("cls");
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 9);
	for (int i = 0; i < width + 2; i++)
		std::cout << "-";
	std::cout << "\n";

	for (int i = 0; i < height; i++) {
		for (int j = 0; j < width; j++) {
			if (j == 0)
				std::cout << "|";
			if (i == snakeY && j == snakeX)
				std::cout << "C";
			else
				if (i == fruitY && j == fruitX)
					std::cout << "F";
				else
				{
					bool print = false;
					for (int k = 0; k < nTail; k++) {
						if (tailX[k] == j && tailY[k] == i) {
							std::cout << "o";
							print = true;
						}
					}
					if (!print)
						std::cout << " ";
				}

			if (j == width - 1)
				std::cout << "|";
		}
		std::cout << "\n";
	}

	for (int i = 0; i < width + 2; i++)
		std::cout << "-";
	std::cout << "\n";

	std::cout << "Score is: " << score;
}

void Snake::input() {
	if (_kbhit()) {
		switch (_getch()) {
		case 'a': {
			dir = LEFT;
			break;
		}
		case 'd': {
			dir = RIGHT;
			break;
		}
		case 'w': {
			dir = UP;
			break;
		}
		case 's': {
			dir = DOWN;
			break;
		}
		case 'x': {
			gameOver = true;
			break;
		}
		}
	}
}

void Snake::logic() {
	int prevX = tailX[0];
	int prevY = tailY[0];
	int prevX2, prevY2;
	tailX[0] = snakeX;
	tailY[0] = snakeY;
	for (int i = 1; i < nTail; i++)
	{
		prevX2 = tailX[i];
		prevY2 = tailY[i];
		tailX[i] = prevX;
		tailY[i] = prevY;
		prevX = prevX2;
		prevY = prevY2;
	}

	switch (dir) {
	case LEFT: {
		snakeX--;
		break;
	}
	case RIGHT: {
		snakeX++;
		break;
	}
	case UP: {
		snakeY--;
		break;
	}
	case DOWN: {
		snakeY++;
		break;
	}
	default:
		break;
	}

	if (snakeX >= width)
		snakeX = 0;
	else
		if (snakeX < 0)
			snakeX = width - 1;
	if (snakeY >= height)
		snakeY = 0;
	else
		if (snakeY < 0)
			snakeY = height - 1;

	for (int i = 0; i < nTail; i++)
		if (tailX[i] == snakeX && tailY[i] == snakeY)
			gameOver = true;

	if (snakeX == fruitX && snakeY == fruitY) {
		score++;
		fruitX = rand() % width;
		fruitY = rand() % height;
		nTail++;
	}
}

bool Snake::getGameOver() {
	return gameOver;
}